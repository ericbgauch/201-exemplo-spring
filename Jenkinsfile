pipeline {
    agent any
    stages {
        stage("Build") {
            steps {
                sh "./mvnw clean package"
            }
        }

        stage("Sonar") {
            steps {
                withSonarQubeEnv("sonarqube-local") {
                    sh "./mvnw clean org.jacoco:jacoco-maven-plugin:prepare-agent test -Dmaven.test.failure.ignore=false sonar:sonar"
                }
            }
        }

        stage("Integration") {
            steps {
                script {
                    docker.image("mysql:latest").withRun(
                    "-e MYSQL_ROOT_PASSWORD='@a2' " +
                    "-e MYSQL_DATABASE='estoque_test' " +
                    "-p 3306:3306") { c ->
                        sh "while ! mysqladmin ping -h 0.0.0.0 -u root -p'@a2' --silent; do sleep 1; done"
                        sh "./mvnw clean verify"
                    }
                }
            }
        }

        stage("Package") {
            steps {
                script {
                    def artifactFolder = "/var/lib/jenkins/artifacts"
                    def needTargetPath = !fileExists("${artifactFolder}")
                    if(needTargetPath) {
                        sh "mkdir ${artifactFolder}"
                    }
                    def targetPath = sh (
                        script: "find target/ -name 'produto-*.jar'",
                        returnStdout: true
                    ).trim()
                    sh "cp ${targetPath} ${artifactFolder}"
                }
            }
        }

        stage("Deliver Artifact") {
            steps {
                script {
                    def artifactFolder = "/var/lib/jenkins/artifacts"
                    def targetPath = sh (
                        script: "find ${artifactFolder}/ -name 'produto-*.jar'",
                        returnStdout: true
                    ).trim()
                    sh "scp -o StrictHostKeyChecking=no ${targetPath} itau@${env.HOST}:/home/itau/"
                }
            }
        }

        stage("Deploy Artifact on Server") {
            steps {
                script {
                    def targetPath = sh (
                        script: "find ${artifactFolder}/ -name 'produto-*.jar'",
                        returnStdout: true
                    ).trim()
                    sh "ssh -t itau@${env.HOST} 'java -jar ${targetPath}'"
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
