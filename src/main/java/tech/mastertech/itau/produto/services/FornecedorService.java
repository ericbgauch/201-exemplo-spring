package tech.mastertech.itau.produto.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.produto.models.Fornecedor;
import tech.mastertech.itau.produto.repositories.FornecedorRepository;

@Service
public class FornecedorService {
  @Autowired
  private FornecedorRepository fornecedorRepository;
  
  public Fornecedor setFornecedor(Fornecedor fornecedor) {
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    
    fornecedor.setSenha(encoder.encode(fornecedor.getSenha()));
    
    return fornecedorRepository.save(fornecedor);
  }
  
  public Iterable<Fornecedor> getFornecedores(){
    return fornecedorRepository.findAll();
  }
  
  public Fornecedor fazerLogin(Fornecedor fornecedor){
    Optional<Fornecedor> fornecedorOptional = 
        fornecedorRepository.findByNomeUsuario(fornecedor.getNomeUsuario());
    
    if(!fornecedorOptional.isPresent()) {
      return null;
    }
    
    Fornecedor fornecedorSalvo = fornecedorOptional.get();
    
    BCryptPasswordEncoder bPasswordEncoder = new BCryptPasswordEncoder();
    
    if(bPasswordEncoder.matches(fornecedor.getSenha(), fornecedorSalvo.getSenha())) {
      return fornecedorSalvo;
    }
    
    return null;
  }
}
